## vmrc (kinetic) - 0.3.0-0

The packages in the `vmrc` repository were released into the `kinetic` distro by running `/usr/local/bin/bloom-release --rosdistro kinetic --track kinetic vmrc --edit` on `Fri, 28 Sep 2018 21:06:12 -0000`

These packages were released:
- `usv_gazebo_plugins`
- `vmrc_gazebo`
- `wamv_description`
- `wamv_gazebo`

Version of package(s) in repository `vmrc`:

- upstream repository: https://bitbucket.org/osrf/vmrc
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `0.3.0-0`

Versions of tools used:

- bloom version: `0.6.7`
- catkin_pkg version: `0.4.3`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.9`
- vcstools version: `0.1.40`


# vmrc-release

